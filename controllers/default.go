package controllers

import (
	"MyWebServer/models"
	"github.com/astaxie/beego"
	"log"
)

type MainController struct {
	beego.Controller
}

//检测是否登陆
func (c *MainController) Prepare() {
	sess := c.StartSession()
	sess_username := sess.Get("username")
	if sess_username == nil {
		c.Ctx.Redirect(302, "/")
		return
	}
}

//显示页面
func (c *MainController) Get() {
	sys := models.GetAll()
	c.Data["MultiAddress"] = sys.MultiAddress
	c.Data["Port"] = sys.Port
	c.Data["UserName"] = sys.UserName
	str := sys.Password
	password, err := models.DecryptParams([]byte(str)) //解密
	if err != nil {
		log.Fatal("解析密码错误", err)
	} else {
		c.Data["Password"] = string(password)
		//c.Data["Website"] = "beego.me"
		//c.Data["Email"] = "astaxie@gmail.com"
		//c.TplNames = "index.tpl"
		c.Layout = "layout.html"
		c.TplNames = "home.html"
	}
	//c.Data["Password"] = str
	//c.Layout = "layout.html"
	//c.TplNames = "home.html"
}

//处理提交请求
func (c *MainController) Post() {
	inputs := c.Input()
	var sys models.SysInfo
	sys.MultiAddress = inputs.Get("multiaddress")
	sys.Port = inputs.Get("port")
	sys.UserName = inputs.Get("username")
	password := models.EncryptionParams([]byte(inputs.Get("password"))) //base64加密
	sys.Password = password
	//sys.Password = inputs.Get("password")
	models.SaveSys(sys)
	c.Ctx.Redirect(302, "/home") //页面跳转
}
