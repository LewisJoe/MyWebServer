package main

import (
	_ "MyWebServer/routers"
	"github.com/astaxie/beego"
	"github.com/googollee/go-socket.io"
	"log"
	"os/exec"
	"reflect"
)

func main() {
	server, err := socketio.NewServer(nil)
	if err != nil {
		log.Fatal(err)
	}
	//监听客户端链接
	server.On("connection", func(so socketio.Socket) {
		log.Println("on connection")
		//监听客户端发送的信息
		so.On("change-state", func(msg interface{}, port, multiAddress string) {
			log.Println("The status is:", msg)
			//log.Println(reflect.TypeOf(msg))//查看msg的类型
			if msg == true {
				log.Println("The port is:", port)
				log.Println("The address is:", multiAddress)
				log.Println("The type of port is:", reflect.TypeOf(port))
				log.Println("The type of address is:", reflect.TypeOf(multiAddress))
				bCmd := exec.Command("bash", "running.sh", port, multiAddress) //启动bash文件并传递参数
				bOut, err := bCmd.Output()
				if err != nil {
					panic(err)
				}
				log.Println(string(bOut))
			} else {
				log.Println("Close running program!")
				cCmd := exec.Command("bash", "running_close.sh")
				cOut, err := cCmd.Output()
				if err != nil {
					panic(err)
				}
				log.Println(string(cOut))
			}
		})
		so.On("disconnection", func() {
			log.Println("on disconnection")
		})
	})
	server.On("error", func(so socketio.Socket, err error) {
		log.Println("Error:", err)
	})
	beego.Handler("/socket.io/", server)
	beego.Run()
}
