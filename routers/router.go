package routers

import (
	"MyWebServer/controllers"  //自身业务包
	"github.com/astaxie/beego" //beego包
)

func init() {
	beego.Router("/home", &controllers.MainController{}) //主页
	beego.Router("/", &controllers.LoginController{})    //登陆页面
}
