package models

import (
	"encoding/base64"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

//xml结构体
type SysInfo struct {
	//XMLName      xml.Name `xml:"config"`
	UserName     string `xml:"userName"`     //用户名
	Password     string `xml:"password"`     //密码
	MultiAddress string `xml:"multiAddress"` //组播地址
	Port         string `xml:"port"`         //端口号
}

//读取xml文件获取配置信息
func GetAll() SysInfo {
	file, err := os.Open("./conf/sys.xml")
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	defer file.Close()
	data, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatal("Read sys.xml error!", err)
		os.Exit(1)
	}
	sys := SysInfo{}
	err = xml.Unmarshal(data, &sys)
	if err != nil {
		log.Fatal("Unmarshal sys.xml error!", err)
		os.Exit(1)
	}
	return sys
}

//保存配置信息到xml文件
func SaveSys(sys SysInfo) {
	output, err := xml.MarshalIndent(sys, "", "	")
	if err != nil {
		panic(err)
	}
	file, err := os.OpenFile("./conf/sys.xml", os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0x777) //读写创建文件清空里面的数据
	if err != nil {
		panic(err)
	}
	defer file.Close()
	wstr, err := file.Write(output)
	if err != nil {
		panic(err)
	}
	fmt.Println(wstr)
}

//将密码进行base64加密
func EncryptionParams(src []byte) string {
	return base64.StdEncoding.EncodeToString(src)
}

//将密码进行base64解密
func DecryptParams(src []byte) ([]byte, error) {
	return base64.StdEncoding.DecodeString(string(src))
}
