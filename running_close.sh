#!/bin/bash
if [[ "$1" = "" ]]; then
	echo "Error! Running program is empty!"
	exit 0
fi
cmd=`ps -ef|grep $1|grep -v "bash $1"|awk '{print $2}'`
echo "$cmd"|while read pid; do
	if [[ "$pid" != "" ]]; then
		echo "the pid is $pid"
	else
		echo "the pid is null!"
	fi
done